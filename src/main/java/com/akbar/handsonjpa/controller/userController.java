package com.akbar.handsonjpa.controller;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.akbar.handsonjpa.entity.User;
import com.akbar.handsonjpa.repository.UserRepository;
import com.akbar.handsonjpa.response.HttpResponse;
import com.google.common.hash.Hashing;

@RestController
public class userController {

	@Autowired
	UserRepository repo;
	
	// ADD NEW USER
	@PostMapping(value = "api/user", consumes = "application/json", produces = "application/json")
	public HttpResponse addUser(@RequestBody User user) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		if(isExistUsername(user))
			return new HttpResponse(HttpStatus.OK.value(), "Username already exists");
		@SuppressWarnings("unused")
		int rowAffected = repo.addUser(user.getUsername(), encrypt(user.getPassword()));
		User addedUser = repo.getUserByUsername(user.getUsername());
		return new HttpResponse(HttpStatus.CREATED.value(), "New user created", hidePasswordUser(addedUser));
	}
	
	private boolean isExistUsername(User user) {
		return (repo.getUserByUsername(user.getUsername())!=null);
	}
	
	private String encrypt(String message) {
		return Hashing.sha256().hashString(message, StandardCharsets.UTF_8).toString();
	}
	
	// GET ALL USERS
	@GetMapping(value = "api/users", produces = "application/json")
	public HttpResponse getUsers() {
		List<User> users = repo.getAll();
		if(users.size() == 0)
			return new HttpResponse(HttpStatus.NO_CONTENT.value(), "Empty result");
		
		return new HttpResponse(HttpStatus.OK.value(), "Success", hidePasswordUsers(users));
	}
	
	private List<User> hidePasswordUsers(List<User> users){
		List<User> newHiddenUsers = new ArrayList<User>();
		users.forEach(user -> {
			User newHiddenUser = new User(user.getId(), user.getUsername());
			newHiddenUsers.add(newHiddenUser);
		});
		return newHiddenUsers;
	}
	
	// GET USER BY ID
	@GetMapping(value = "api/user/{id}", produces = "application/json")
	public HttpResponse getUserById(@PathVariable String id) {
		User user = repo.getById(id);
		if(user == null)
			return new HttpResponse(HttpStatus.NO_CONTENT.value(), "User not found");
		
		return new HttpResponse(HttpStatus.OK.value(), "Success", hidePasswordUser(user));
	}
	
	private User hidePasswordUser(User user){
		return new User(user.getId(), user.getUsername());
	}
		
	//AUTH
	@PostMapping(value = "api/auth/login", consumes = "application/json", produces = "application/json")
	public HttpResponse auth(@RequestBody User user) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		if(!isExistUsername(user) || !isPasswordValid(user))
			return new HttpResponse(HttpStatus.OK.value(), "Invalid username or password ");
		return new HttpResponse(HttpStatus.OK.value(), "Success Login");
	}

	private Boolean isPasswordValid(User user) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		User userFromDB = repo.getUserByUsername(user.getUsername());
		if(!encrypt(user.getPassword()).equals(userFromDB.getPassword()))
			return false;
		return true;
	}
}

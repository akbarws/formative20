package com.akbar.handsonjpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.akbar.handsonjpa.entity.User;

@Transactional
public interface UserRepository extends JpaRepository<User, Long> {

	@Query(value = "SELECT * FROM user WHERE username=:username", nativeQuery = true)
	User getUserByUsername(String username);
	
	@Modifying
	@Query(value = "INSERT INTO user VALUES (null, :username, :password)", nativeQuery = true)
	int addUser(@Param("username") String username, @Param("password") String password);

	@Query(value = "SELECT * FROM user", nativeQuery = true)
	List<User> getAll();

	@Query(value = "SELECT * FROM user WHERE id=:id", nativeQuery = true)
	User getById(@Param("id") String id);
}

package com.akbar.handsonjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HandsonJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(HandsonJpaApplication.class, args);
	}

}
